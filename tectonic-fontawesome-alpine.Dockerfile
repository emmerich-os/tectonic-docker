ARG VERSION

FROM dxjoke/tectonic-docker:${VERSION}-alpine-biber

RUN apk update \
    && apk add --no-cache \
    ttf-font-awesome \
    # texmf-dist-fontsextra \ 
    # texlive \
    && rm -rf /var/lib/apt/lists/*

