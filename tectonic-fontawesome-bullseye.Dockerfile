ARG VERSION

FROM dxjoke/tectonic-docker:${VERSION}-bullseye-biber

RUN apt-get update \
    && apt-get install -yq --no-install-suggests --no-install-recommends \
    fonts-font-awesome \
    #texlive-fonts-extra \
    #texlive-latex-recommended \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

